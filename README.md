# CI-NIFI
![Projet 2 - CI-NIFI](https://img.shields.io/badge/Projet_2-CI--NiFi-2ea44f?logo=prometheus&logoColor=white)
![Ansible - Playbooks](https://img.shields.io/badge/Ansible-Playbooks-2ea44f?logo=Ansible)
![Docker - Compose](https://img.shields.io/badge/Docker-Compose-blue?logo=docker&logoColor=white)
![Hashicorp - Vagrant](https://img.shields.io/badge/Hashicorp-Vagrant-white?logo=vagrant&logoColor=blue)

## Sujet du projet:

>*Introduction :*
Le projet consiste à déployer une application NiFi sur Docker à l'aide d'Ansible. L'utilisation de Docker permet de créer un environnement isolé et portable pour l'application, tandis qu'Ansible facilite la configuration et le déploiement de l'application. Dans ce projet, nous allons installer Docker et Ansible, configurer Docker pour NiFi, créer un playbook Ansible pour le déploiement de NiFi sur Docker, et exécuter le playbook pour déployer NiFi sur les conteneurs Docker.

>*Consignes :*
1- Installation de Docker et Ansible :
- Téléchargez et installez Docker sur votre machine locale ou sur votre environnement cloud.
- Installez Ansible sur votre machine locale.
2- Configuration de Docker :
- Configurez un environnement Docker avec un ou plusieurs noeuds.
- Créez un conteneur pour NiFi.
3- Configuration d'Ansible
- Créez un playbook Ansible pour le déploiement de NiFi sur Docker.
- Ajoutez les tâches nécessaires pour créer les conteneurs Docker, tels que les images Docker, les volumes, les ports, etc.
- Utilisez des rôles Ansible pour simplifier la configuration et la maintenance du playbook.
4- Déploiement de NiFi sur Docker avec Ansible :
- Exécutez le playbook Ansible pour déployer NiFi sur un conteneur Docker.
- Vérifiez que le conteneur Docker a été créé avec succès et que NiFi fonctionne correctement
5- Orchestration du déploiement avec Gitlab-CI
- Implémenter le déploiement de NiFi sur Docker avec Ansible dans Gitlab-CI en spécifiant pour chaque nouveau déploiement un nouveau nom de conteneur et un nouveau port.

## Installation du système:
- vagrant up --provision
- configurer le runner
- Enjoy ;)

## Fonctionnement du système:
- Aller dans Gitlab, dans pipeline, lancer une nouvelle pipeline
- remplir NIFI_PORT et NIFI_NAME, et lancer
- le nouveau container a été créé

## TODO
### 1 Installation de Docker et Ansible
- [x] A partir du vagrantFile, installer Docker et Ansible

### 2 Configuration du docker
- [x] Configurer un docker compose pour un docker qui gère plusieurs noeuds
- [x] Créer un containeur sous l'image NiFi

### 3 Configuration d'Ansible
- [x] Créer un playbook pour déploiement de NiFi sur docker
- [x] Ajoutez les tâches nécessaires pour créer les conteneurs Docker, tels que les images Docker, les volumes, les ports, etc.
- [x] Utilisez des rôles Ansible pour simplifier la configuration et la maintenance du playbook.

### 4 Deploiement de NiFi sur Docker avec Ansible
- [x] Exécutez le playbook Ansible pour déployer NiFi sur un conteneur Docker.
- [x] Vérifiez que le conteneur Docker a été créé avec succès et que NiFi fonctionne correctement

### 5 Orchestration du déploiement avec Gitlab-CI
- [x] Mettre en place un serveur Gitlab dans un conteneur
- [x] Créer un projet "NiFi"
- [x] Mettre en place un runners qui lancera le playbook
- [x] Créer une pipeline qui exécute le playbook ansible
